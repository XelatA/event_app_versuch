class CreateJobs < ActiveRecord::Migration
  def change
    create_table :jobs do |t|
      t.integer :event_id
      t.string :name
      t.string :jobtext
      t.float :processing_time
      t.integer :ressource_demand

      t.timestamps null: false
    end
  end
end
