class PeriodsController < ApplicationController
  before_action :set_period, only: [:show, :edit, :update, :destroy]

  # GET /periods
  # GET /periods.json
  def index
    @periods = Period.all
    @number_of_periods = Period.count
    @sum_add_cappa=Period.sum('add_cappa')
    if @mynumber_of_periods = nil
      @mynumber_of_periods=@number_of_periods
    end

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @periods }
    end
  end

  # GET /periods/1
  # GET /periods/1.json
  def show
  end

  # GET /periods/new
  def new
    @period = Period.new
  end

  # GET /periods/1/edit
  def edit
  end

  # POST /periods
  # POST /periods.json
  def create
    @period = Period.new(period_params)

    respond_to do |format|
      if @period.save
        format.html { redirect_to @period, notice: 'Period was successfully created.' }
        format.json { render :show, status: :created, location: @period }
      else
        format.html { render :new }
        format.json { render json: @period.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /periods/1
  # PATCH/PUT /periods/1.json
  def update
    respond_to do |format|
      if @period.update(period_params)
        format.html { redirect_to @period, notice: 'Period was successfully updated.' }
        format.json { render :show, status: :ok, location: @period }
      else
        format.html { render :edit }
        format.json { render json: @period.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /periods/1
  # DELETE /periods/1.json
  def destroy
    @period.destroy
    respond_to do |format|
      format.html { redirect_to periods_url, notice: 'Period was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def change
    @mynumber_of_periods = params[:mynewnumber].to_i
    @number_of_periods = Period.count
    (0..@number_of_periods-1).each do |number|
      name="t#{number}"
      Period.find_by_name(name).destroy
    end
    @events = Event.all
    @jobs = Job.all
    (0..@mynumber_of_periods).each do |number|
      name="t#{number}" and
          date=Time.now+number.days
      @new_period=Period.create!(name: name, time: date)
      #@jobs.each{ |jo|
      #  EventJobPeriodAssociation.create(event_id: jo.event.id, job_id: jo.id, period_id: @new_period.id)
      #}
    end
    @periods = Period.all
    redirect_to :action => :index
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_period
      @period = Period.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def period_params
      params.require(:period).permit(:name, :time, :add_cappa)
    end
end
