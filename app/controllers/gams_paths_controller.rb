class GamsPathsController < ApplicationController
  before_action :set_gams_path, only: [:show, :edit, :update, :destroy]

  # GET /gams_paths
  # GET /gams_paths.json
  def index
    @gams_paths = GamsPath.all
    respond_to do |format|
      format.html
    end
  end

  # GET /gams_paths/1
  # GET /gams_paths/1.json
  def show
  end

  # GET /gams_paths/new
  def new
    @gams_path = GamsPath.new
  end

  # GET /gams_paths/1/edit
  def edit
  end

  # POST /gams_paths
  # POST /gams_paths.json
  def create
    @gams_path = GamsPath.new(gams_path_params)

    respond_to do |format|
      if @gams_path.save
        format.html { redirect_to @gams_path, notice: 'Gams path was successfully created.' }
        format.json { render :show, status: :created, location: @gams_path }
      else
        format.html { render :new }
        format.json { render json: @gams_path.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /gams_paths/1
  # PATCH/PUT /gams_paths/1.json
  def update
    respond_to do |format|
      if @gams_path.update(gams_path_params)
        format.html { redirect_to @gams_path, notice: 'Gams path was successfully updated.' }
        format.json { render :show, status: :ok, location: @gams_path }
      else
        format.html { render :edit }
        format.json { render json: @gams_path.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /gams_paths/1
  # DELETE /gams_paths/1.json
  def destroy
    @gams_path.destroy
    respond_to do |format|
      format.html { redirect_to gams_paths_url, notice: 'Gams path was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_gams_path
      @gams_path = GamsPath.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def gams_path_params
      params.require(:gams_path).permit(:gams_path_url)
    end
end
